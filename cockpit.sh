#!/bin/bash

# cockpit Installation
sudo apt install cockpit apache2
sudo systemctl enable cockpit.socket

# Cockpit Apache Proxy configuration
# Ref: https://github.com/cockpit-project/cockpit/wiki/Proxying-Cockpit-over-Apache-with-LetsEncrypt
read -p "Enter Domain Name: " dns
read -p "Enter Port number: " port
sudo touch /etc/apache2/sites-available/$dns.conf
echo "<VirtualHost *:80>
	ServerName dns

        ProxyPreserveHost on
        ProxyRequests off
        ProxyPass               /       http://localhost:port/
        ProxyPassReverse        /       http://localhost:port/

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>" | sudo tee -a /etc/apache2/sites-available/$dns.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns.conf
sudo sed -i "s/port/$port/g" /etc/apache2/sites-available/$dns.conf
sudo a2ensite $dns.conf
sudo service apache2 restart

# Certbot Installation
sudo snap install core; sudo snap refresh core
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly --apache

# Apache SSL Configuration
sudo a2enmod proxy proxy_http proxy_wstunnel ssl rewrite
sudo touch /etc/apache2/sites-available/$dns-ssl.conf
echo "<IfModule mod_ssl.c>
        <VirtualHost *:443>
                ServerName dns
		ServerAlias www.dns

                SSLEngine on
                SSLCertificateFile      /etc/letsencrypt/live/dns/fullchain.pem
                SSLCertificateKeyFile	/etc/letsencrypt/live/dns/privkey.pem
                Include /etc/letsencrypt/options-ssl-apache.conf

                # allow for upgrading to websockets
                RewriteEngine On
                RewriteCond %{HTTP:Upgrade} =websocket [NC]
                RewriteRule /(.*)           ws://127.0.0.1:port/$1 [P,L]
                RewriteCond %{HTTP:Upgrade} !=websocket [NC]
                RewriteRule /(.*)           http://127.0.0.1:port/$1 [P,L]

                ProxyPreserveHost on
                ProxyRequests off
                ProxyPass               /       http://localhost:port/
                ProxyPassReverse        /       http://localhost:port/

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

        </VirtualHost>
</IfModule>" | sudo tee -a /etc/apache2/sites-available/$dns-ssl.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns-ssl.conf
sudo sed -i "s/port/$port/g" /etc/apache2/sites-available/$dns.conf
sudo a2ensite $dns-ssl.conf
sudo service apache2 restart

# Cockpit configuration
sudo cp /etc/letsencrypt/live/$dns/fullchain.pem /etc/cockpit/ws-certs.d/0-cockpit.cert
sudo cat /etc/letsencrypt/live/$dns/privkey.pem /etc/cockpit/ws-certs.d/0-cockpit.key
sudo touch /etc/cockpit/cockpit.conf
echo "[WebService]
Origins = https://dns http://localhost:port
ProtocolHeader = X-Forwarded-Proto
AllowUnencrypted = true" | sudo tee -a /etc/cockpit/cockpit.conf
sudo sed -i "s/dns/$dns/g" /etc/cockpit/cockpit.conf
sudo service cockpit restart

# To make updates work
sudo nmcli con add type dummy con-name fake ifname fake0 ip4 1.2.3.4/24 gw4 1.2.3.1
sudo service NetworkManager restart

# Final
echo "Cockpit Installation and config complete
Open https://$dns"
read -p	"Press enter to exit" bye
kill -9 $PPID
