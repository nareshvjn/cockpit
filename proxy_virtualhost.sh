# Adding proxy virtual host
read -p "Enter Domain Name: " dns
read -p "Enter Port number: " port
sudo touch /etc/apache2/sites-available/$dns.conf
echo "<VirtualHost *:80>
	ServerName dns

        ProxyPreserveHost on
        ProxyRequests off
        ProxyPass               /       http://localhost:port/
        ProxyPassReverse        /       http://localhost:port/

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>" | sudo tee -a /etc/apache2/sites-available/$dns.conf > /dev/null
sudo sed -i "s/dns/$dns/g" /etc/apache2/sites-available/$dns.conf
sudo sed -i "s/port/$port/g" /etc/apache2/sites-available/$dns.conf
sudo a2ensite $dns.conf
sudo service apache2 restart
